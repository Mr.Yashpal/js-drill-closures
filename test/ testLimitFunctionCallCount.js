import { limitFunctionCallCount } from '../limitFunctionCallCount.js';

function cb() {
    console.log("invoked");
}

let n = 3;
var call = limitFunctionCallCount(cb, n);

// call.invoke(); // n=3 | invoked 
// call.invoke(); // n=2 | invoked 
// call.invoke(); // n=1 | invoked 
// call.invoke(); // n=0 | Not invoked 
// call.invoke(); // n=0 | Not invoked