import { counterFactory } from '../counterFactory.js';

if (counterFactory.increment() == 1 && counterFactory.decrement() == 0)
    console.log("Test Passed");
else
    console.log("Test Failed");