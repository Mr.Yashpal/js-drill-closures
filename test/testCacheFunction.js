import { cacheFunction } from '../cacheFunction.js';

function cb(value) {
    return value * value;
}

var call = cacheFunction(cb);

if (call.invoke(2) == 4 && call.invoke(3) == 9)
    console.log("Test Passed");
else
    console.log("Test Failed");