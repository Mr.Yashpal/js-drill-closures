// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

export function limitFunctionCallCount(cb, n) {
    return {
        invoke: function () {
            if (n > 0) {
                n -= 1;
                cb();
            }
        }
    };
};